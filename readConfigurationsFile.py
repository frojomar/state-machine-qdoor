

def loadConfigurations(file_name, configurations):
    f= open(file_name, "r")
    num_linea= 1
    for line in f:
        line= line.replace(" ", "")
        if line.find('#') == 0 or line.__eq__("\n"):
            None
        else:
            if line.find('=') == -1:
                print('[ERROR] '+str(file_name)+' is not a valid configuration file')
                print('Error in line '+str(num_linea))
                print('Config value is not valid ('+str(line)+')')
            else:
                name= line.split('=')[0]
                value=line.split('=')[1]
                if line.split('=').__len__() > 2:
                    for sub in line.split('=')[2:]:
                        value= str(value).__add__('=').__add__(sub)
                value= value.replace("\n", "")
                configurations[name]=value
        num_linea=num_linea+1