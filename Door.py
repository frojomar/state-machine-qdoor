import socket
import sys
import urllib
import base64
import os
import requests
import threading
import time
from datetime import date
import signal
import readConfigurationsFile as conf
import cv2
import sys
import json

import shutil
import datetime

configurations = {}
if len(sys.argv)> 1:
    conf.loadConfigurations(str(sys.argv[1]), configurations)
else:
    conf.loadConfigurations('settings.conf', configurations)
print(configurations)

if len(sys.argv)> 2:
    PATH=str(sys.argv[2])
else:
    PATH="."

TIEMPO_RESET = 15
TIEMPO_VOLVER_A_CONTAR = 10
ACTUADOR_ENTRADA = str(configurations['ACTUADOR_ENTRADA']).replace(" ", "")
ACTUADOR_SALIDA = str(configurations['ACTUADOR_SALIDA']).replace(" ", "")
CONTADOR = str(configurations['PUSH_CONTADOR']).replace(" ", "")
HOST = str(configurations['HOST']).replace(" ", "")
PORT_ENTRADA = str(configurations['PORT_ENTRADA']).replace(" ", "")
PORT_SALIDA = str(configurations['PORT_SALIDA']).replace(" ", "")
USER_CAMERA = str(configurations['USUARIO']).replace(" ", "")
PASS_CAMERA = str(configurations['PASSWORD']).replace(" ", "")
URL_FLUJO_VIDEO = str(configurations['URL_CAMARA']).replace(" ", "")
ID_CAMERA = str(configurations['ID_CAMERA'])

REC_ADDRESS = str(configurations['REC_ADDRESS']).replace(" ", "")
YOLO_ADDRESS = str(configurations['YOLO_ADDRESS']).replace(" ", "")


entrando = False
saliendo = False
contadorEntrada = 0
contadorSalida = 0
contadorEntradaHoy = 0
contadorSalidaHoy = 0
contadorDentro = 0
TimeStamp = -1
inc = 0
id_persons=[]
image_reconocedor=""
no_rec=False





my_semaphore = threading.Semaphore(1)

def newDay():
    global actualDay

    return date.today() > actualDay


def loadFile():
    global actualDay
    global contadorDentro

    try:
        with open(str(PATH)+"/status+"+str(ID_CAMERA)+".txt", 'r') as r:
            date=str(r.readline()).replace(" ", "").replace("\n", "")
            print(date)
            actualDay=datetime.datetime.strptime(date, "%m/%d/%Y,%H:%M:%S").date()
            contadorDentro=int(r.readline())
            r.close()
    except FileNotFoundError:
        print("No se ha encontrado un fichero con valores previos")


def my_handler(signum, frame):
    global actualDay
    print("Signal '" + str(signum) + "' recieved. Killing all process")

    with open(str(PATH)+"/status"+str(ID_CAMERA)+".txt", 'w') as w:
        w.write(actualDay.strftime("%m/%d/%Y,%H:%M:%S")+"\n")
        w.write(str(contadorDentro)+"\n")
        w.close()


def reset():
    global entrando
    global saliendo
    global my_semaphore
    global TimeStamp

    pid3 = os.getpid()
    time.sleep(5) #espera inicial, antes de comenzar a trabajar

    print(threading.current_thread().getName() + "::: reset INICIADO")

    # my_mutex.acquire()
    my_semaphore.acquire()

    while True:
        if (entrando or saliendo) and (time.time() - TimeStamp > TIEMPO_VOLVER_A_CONTAR):
            print(threading.current_thread().getName() + "::: " + str(entrando) + " - " + str(saliendo))
            entrando = False
            saliendo = False
            print(threading.current_thread().getName() + "::: reseteadas las variables")
        my_semaphore.release()
        time.sleep(TIEMPO_RESET)
        my_semaphore.acquire()

    my_semaphore.release()


def actuador_entrada():
    if ACTUADOR_ENTRADA and str(ACTUADOR_ENTRADA).replace(" ", "") != "":
        print('>>Persona detectada -- ejecutando actuador entrada')
        requests.get(ACTUADOR_ENTRADA)
    else:
        print('>>Persona detectada -- no hay actuador de entrada definido')



def actuador_salida():
    if ACTUADOR_SALIDA and str(ACTUADOR_SALIDA).replace(" ", "") != "":
        print('>>Persona detectada -- ejecutando actuador salida')
        requests.get(ACTUADOR_SALIDA)
    else:
        print('>>Persona detectada -- no hay actuador de salida definido')



def getAccessLevelBySchedules():

    accessLevelMax = 99
    nowtime=datetime.datetime.now().time()
    schedules=[]
    try:
        host_back = str(configurations['HOST_BACKEND'])
        port_back = str(configurations['PORT_BACKEND'])
        id_camera = str(configurations['ID_CAMERA'])
        response = requests.get(url='http://' + host_back + ':' + port_back + '/camera/'+id_camera+'/schedules/statemachine')
        if response.status_code == 200:
            schedules=response.json()
        elif response.status_code == 204:
            print("No hay reglas de acceso definidas. Acceso abierto")
        else:
            print("[ERROR " + str(response.status_code) + "]NO se han podido consultar las reglas de acceso")
            accessLevelMax = 99
    except Exception as ex:
        print("No se han podido consultar las reglas de acceso")
        accessLevelMax = 99
        print(ex)

    for schedule in schedules:
        scheduleInit=datetime.datetime.strptime(schedule['start_time'], '%H:%M:%S').time()
        scheduleEnd = datetime.datetime.strptime(schedule['end_time'], '%H:%M:%S').time()
        if nowtime < scheduleEnd and nowtime > scheduleInit:
            if int(schedule['access_level_min'])> accessLevelMax or accessLevelMax == 99:
                accessLevelMax = int(schedule['access_level_min'])
                print("Actualizando nivel de acceso a "+str(accessLevelMax))

    return accessLevelMax



def entrada(image_64_encode, NowTime, LastTime):
    global my_semaphore
    global entrando
    global saliendo
    global contadorSalida
    global TimeStamp
    global inc
    global actualDay
    global contadorEntradaHoy
    global contadorSalidaHoy
    global contadorDentro
    global id_persons
    global image_reconocedor
    global no_rec

    print(threading.current_thread().getName() + "::: EMPEZANDO")

    my_semaphore.acquire()

    print(threading.current_thread().getName() + "::: semaforo adquirido")
    no_rec=False
    try:

        response = requests.post(YOLO_ADDRESS, data=image_64_encode)
        #print (response.text)

        howmany = 0
        print("Numero de personas: "+str((response.text).count('person')))

        if 'person' in response.text:
            howmany = (response.text).count('person')
            print(">Persona detectada --> " + str(howmany) + " personas detectadas")

            if not entrando and not saliendo:
                accessLevelActual = getAccessLevelBySchedules()

                acceso = False
                if accessLevelActual == 0:  # todos
                    acceso = True
                    print("Acceso abierto a todo el mundo")
                else:
                    print("[ENTRADA] Nivel de acceso necesario: " + str(accessLevelActual))
                    print("[ENTRADA] Consultando el reconocedor facial")
                    id_persons, image_reconocedor, maxAccessLevelPerson = reconocerPersona(image_64_encode)
                    print(id_persons)
                    print(maxAccessLevelPerson)

                    if len(id_persons) > 0:
                        if len(id_persons) > howmany:
                            howmany = len(id_persons)
                        if maxAccessLevelPerson >= accessLevelActual:
                            acceso = True
                        else:
                            no_rec=True
                    else:
                        no_rec=True

                if acceso == True:
                    entrando = True
                    TimeStamp = time.time()
                    actuador_entrada() #lanzando actuador
                    inc = howmany
            if not entrando and saliendo:
                saliendo = False
                if newDay() is True:
                    contadorEntradaHoy = 0
                    contadorSalidaHoy = 0
                    contadorDentro = 0
                    actualDay = date.today()
                contadorSalida += inc
                contadorSalidaHoy += inc
                contadorDentro -= inc
                if contadorDentro < 0:
                    contadorDentro = 0

                print(">>>>>>>>>>>>>>>> [" + str(actualDay) + "]-Personas dentro del edificio: " + str(contadorDentro))
                print(">>>>>>>>>>>>>>>>Personas que han entrado: " + str(contadorEntrada))
                print(">>>>>>>>>>>>>>>>Personas que han salido: " + str(contadorSalida))
                print(">>>>>>>>>>>>>>>> [" + str(actualDay) + "]-Personas que han entrado HOY: " + str(contadorEntradaHoy))
                print(">>>>>>>>>>>>>>>> [" + str(actualDay) + "]-Personas que han salido HOY: " + str(contadorSalidaHoy))

                try:
                    if CONTADOR and str(CONTADOR).replace(" ", "") != "":
                        url_contador=str(CONTADOR).replace("[CONTADOR]", str(contadorDentro))
                        print(url_contador)
                        response = requests.get(url_contador)
                except Exception as ex:
                    print(ex)
                    print ("No se ha podido pushear el contador")


            LastTime = NowTime

    except:
        print(">> NO se ha podido contactar con PyYolo")

    my_semaphore.release()

    print(threading.current_thread().getName() + "::: semaforo soltado :" + str(entrando) + " - " + str(saliendo))

    return LastTime


def salida(image_64_encode, NowTime, LastTime):
    global my_semaphore
    global entrando
    global saliendo
    global contadorEntrada
    global TimeStamp
    global inc
    global contadorEntradaHoy
    global contadorSalidaHoy
    global actualDay
    global contadorDentro
    global id_persons
    global image_reconocedor
    global no_rec

    print(threading.current_thread().getName() + "::: EMPEZANDO")

    my_semaphore.acquire()

    print(threading.current_thread().getName() + "::: semaforo adquirido")

    howmany = 0
    try:
        response = requests.post(YOLO_ADDRESS, data=image_64_encode)
        #print (response.text)

        if 'person' in response.text:
            howmany = (response.text).count('person')
            print(">Persona detectada --> " + str(howmany) + " personas detectadas")
    except:
        print("No se ha podido contactar con PyYolo")

    if howmany<=0:
    	print("No se han detectado personas")
    else:
        if not entrando and not saliendo:
            if no_rec == False:
                accessLevelActual = getAccessLevelBySchedules()
                print("[SALIDA] Nivel necesario a esta hora: "+str(accessLevelActual))
                if accessLevelActual < 99:
                    saliendo = True
                    TimeStamp = time.time()
                    actuador_salida() #lanzando actuador
                    inc = howmany
            else:
                no_rec=False

        if entrando and not saliendo:
            entrando = False
            if newDay() is True:
                contadorEntradaHoy = 0
                contadorSalidaHoy = 0
                contadorDentro = 0
                actualDay = date.today()
            contadorEntrada += inc
            contadorEntradaHoy += inc
            contadorDentro += inc

            print(">>>>>>>>>>>>>>>> [" + str(actualDay) + "]-Personas dentro del edificio: " + str(contadorDentro))
            print(">>>>>>>>>>>>>>>>Personas que han entrado: " + str(contadorEntrada))
            print(">>>>>>>>>>>>>>>>Personas que han salido: " + str(contadorSalida))
            print(">>>>>>>>>>>>>>>> [" + str(actualDay) + "]-Personas que han entrado HOY: " + str(contadorEntradaHoy))
            print(">>>>>>>>>>>>>>>> [" + str(actualDay) + "]-Personas que han salido HOY: " + str(contadorSalidaHoy))

            try:
                if CONTADOR and str(CONTADOR).replace(" ", "") != "":
                    url_contador=str(CONTADOR).replace("[CONTADOR]", str(contadorDentro))
                    print(url_contador)
                    response = requests.get(url_contador)
            except Exception as ex:
                print(ex)
                print ("No se ha podido pushear el contador")

            print(id_persons)
            #creamos una entrada en el registro del backend
            names=""
            if getAccessLevelBySchedules()>0: #Acceso abierto si es 0 --> no se ejecuta reconocedor
                for index, id in enumerate(id_persons):
                    names= names.__add__(id)
                    if index < len(id_persons)-1:
                        names= names.__add__("###")
            else:
            	image_reconocedor=image_64_encode

            data_json={ 'names': str(names),
                        'image_b64': str(image_reconocedor),
                        'id_camera': int(configurations['ID_CAMERA'])}
            host_back = str(configurations['HOST_BACKEND'])
            port_back = str(configurations['PORT_BACKEND'])
            headers = {'content-type': 'application/json'}
            #print(json.dumps(data_json))
            try:
                response= requests.post(url='http://'+host_back+':'+port_back+'/registry', headers=headers, data=json.dumps(data_json))
                if response.status_code == 201 or response.status_code == 200:
                    print("Añadida entrada al registro")
                else:
                    print("[ERROR "+str(response.status_code)+"]NO se ha podido añadir la entrada al registro")
            except Exception as ex:
                print("No se ha podido crear entrada en el registro")
                print(ex)
        LastTime=NowTime


    my_semaphore.release()

    print(threading.current_thread().getName() + "::: semaforo soltado :" + str(entrando) + " - " + str(saliendo))

    return LastTime


def reconocerPersona(image_b64_encode):
    names = []
    image_reconocedor = ""
    accessLevelMax = 0

    #image_procesada=str(image_b64_encode)[2:]
    #image_procesada=image_procesada[:-1]
    image_procesada=str(image_b64_encode)

    with open(str(PATH)+"/b64procesada.txt", 'w') as wr:
    	wr.write(str(image_procesada))
    	wr.close()
    headers = {'content-type': 'application/json'}
    json_data = { 'image': str(image_procesada)}
    try:
        response = requests.post(REC_ADDRESS, headers=headers, data=json.dumps(json_data))
        #print(response.text)
        with open("salida_rec.txt", "w") as wrr:
        	wrr.write(str(response.text))
        	wrr.close()
        if response.status_code== 200:
            json_response=response.json()
            image_reconocedor=json_response['image']
            persons= json_response['persons']
            for person in persons:
                names.append(person['person_id'])
                if int(person['level']) > accessLevelMax:
                    accessLevelMax= int(person['level'])
        else:
            print("[ ERROR "+str(response.status_code)+"]Error consultando al reconocedor facial")

    except Exception as ex:
        print("Error consultando al reconocedor facial")
        print(ex)

    return names, image_reconocedor, accessLevelMax



def receiveNotification(port):
    global howmany

    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the port
    server_address = (str(HOST), port)
    print('starting up on %s port %s \n' % server_address)
    sock.bind(server_address)

    LastTime = time.time()

    # Listen for incoming connections
    sock.listen(1)
    threadName = threading.current_thread().getName()

    while True:
        # Wait for a connection
        print ('waiting for a connection in %s\n' % threadName)
        connection, client_address = sock.accept()

        NowTime = time.time()
        dif = NowTime - LastTime
        print("La diferencia de tiempo es :" + str(dif))

        if dif > 5:
            print ('connection from ', client_address)

            data = connection.recv(16)
            print ('received connection to ', threadName)

            if data:

                if USER_CAMERA !="" and PASS_CAMERA !="":
                    protocolo= str(URL_FLUJO_VIDEO).split('://') [0]
                    url = str(URL_FLUJO_VIDEO).split('://') [1]
                    url_full=str(protocolo)+'://'+str(USER_CAMERA)+":"+str(PASS_CAMERA)+'@'+str(url)
                    print(url_full)
                    video_capture = cv2.VideoCapture(url_full)
                else:
                    video_capture = cv2.VideoCapture(str(URL_FLUJO_VIDEO))


                ret, frame = video_capture.read()

                # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
                #rgb_frame = frame[:, :, ::-1]

                cv2.imwrite(str(PATH)+'/image' + threadName + str(ID_CAMERA)+'.jpg', frame)

                # urllib.urlretrieve('http://juanher:juanher!12@158.49.245.71:27654/jpg/image.jpg',
                #                    'image' + threadName + '.jpg')
                # os.system('/usr/bin/python python-image-resizer.py')
                # with open("peq/image" + threadName.lower() + ".jpg", 'r+') as f:
                print("Cogiendo imagen de la camara")
                with open(str(PATH)+"/image" + threadName + str(ID_CAMERA)+".jpg", 'rb') as f:
                    jpgdata = f.read()
                    f.close()

                    image_64_encode_b = base64.b64encode(jpgdata)
                    image_64_encode = image_64_encode_b.decode('utf-8')

                    with open(str(PATH)+"/b64.txt", 'w') as wr:
                    	wr.write(str(image_64_encode))
                    	wr.close()

                    if port == int(PORT_ENTRADA):
                        LastTime=entrada(image_64_encode, NowTime, LastTime)
                    elif port == int(PORT_SALIDA):
                        LastTime=salida(image_64_encode, NowTime, LastTime)

                # thread.sleep(2) #sleep two seconds, because we want to wait the person is entering or exiting, complete his action
        connection.close()


def saliendoThread():
    global pid1
    pid1 = os.getpid()
    receiveNotification(int(PORT_SALIDA))


def entrandoThread():
    global pid2
    pid2 = os.getpid()
    receiveNotification(int(PORT_ENTRADA))


if __name__ == "__main__":
    global pid1
    global pid2
    global pid3
    global actualDay
    global contadorDentro


    actualDay = date.today()
    contadorDentro=0

    loadFile()

    r = threading.Thread(target=reset, name='ResetThread')
    t = threading.Thread(target=saliendoThread, name='SSSaliendo')
    w = threading.Thread(target=entrandoThread, name='SSEntrando')

    r.start()
    w.start()
    t.start()

    signal.signal(signal.SIGINT, my_handler)

    signal.pause()

    print("Killing '" + w.getName() + "' process (entrando)")
    print(">>Ident:" + str(w.ident) + ". PID:" + str(pid2) + ".")
    os.kill(pid2, signal.SIGTERM)
    print("Killing '" + t.getName() + "' process (saliendo)")
    print(">>Ident:" + str(t.ident) + ". PID:" + str(pid1) + ".")
    os.kill(pid1, signal.SIGTERM)
    print("Killing '" + r.getName() + "' process (reset)")
    print(">>Ident:" + str(r.ident) + ". PID:" + str(pid3) + ".")
    os.kill(pid3, signal.SIGTERM)

