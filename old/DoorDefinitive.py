import socket
import sys
import urllib
import base64
import os
import requests
import threading
import time
from datetime import date
import signal

import shutil
import datetime

entrando = False
saliendo = False
contadorEntrada = 0
contadorSalida = 0
contadorEntradaHoy = 0
contadorSalidaHoy = 0
contadorDentro = 0
TimeStamp = -1
inc = 0

import readConfigurationsFile as conf

configurations = {}
conf.loadConfigurations('settings.conf', configurations)
print(configurations)

my_semaphore = threading.Semaphore(1)


def copy(source, destination):
    path = os.getcwd() + os.sep
    long = 5242880  # buffer of 5MB

    if os.path.exists(source):
        with open(source, 'rb') as forigen:
            with open(destination, 'wb') as fdestino:
                shutil.copyfileobj(forigen, fdestino, long)


def newDay():
    global actualDay
    return date.today() > actualDay


def my_handler(signum, frame):
    print("Signal '" + str(signum) + "' recieved. Killing all process")


def reset():
    global entrando
    global saliendo
    global my_semaphore
    global pid3
    global TimeStamp

    pid3 = os.getpid()
    time.sleep(5)

    print(threading.current_thread().getName() + "::: reset INICIADO")

    # my_mutex.acquire()
    my_semaphore.acquire()

    while True:
        if (entrando or saliendo) and (time.time() - TimeStamp > 10):
            print(threading.current_thread().getName() + "::: " + str(entrando) + " - " + str(saliendo))
            entrando = False
            saliendo = False
            print(threading.current_thread().getName() + "::: reseteadas las variables")
        my_semaphore.release()
        time.sleep(15)
        my_semaphore.acquire()
        # print(threading.current_thread().getName() + "::: semaforos adquiridos")

    my_semaphore.release()


def apertura():
    print('>>Persona detectada -- abriendo puerta')
    requests.get('http://qvs.unex.es:8000/abrirpuerta.php')


def entrada(howmany):
    global my_semaphore
    global entrando
    global saliendo
    global contadorSalida
    global TimeStamp
    global inc
    global actualDay
    global contadorEntradaHoy
    global contadorSalidaHoy
    global contadorDentro

    print(threading.current_thread().getName() + "::: EMPEZANDO")

    my_semaphore.acquire()

    print(threading.current_thread().getName() + "::: semaforo adquirido")

    if not entrando and not saliendo:
        entrando = True
        TimeStamp = time.time()
        apertura()
        inc = howmany
    if not entrando and saliendo:
        saliendo = False
        if newDay() is True:
            contadorEntradaHoy = 0
            contadorSalidaHoy = 0
            contadorDentro = 0
            actualDay = date.today()
        contadorSalida += inc
        contadorSalidaHoy += inc
        contadorDentro -= inc
        if contadorDentro < 0:
            contadorDentro = 0

        print(">>>>>>>>>>>>>>>> [" + str(actualDay) + "]-Personas dentro del edificio: " + str(contadorDentro))
        print(">>>>>>>>>>>>>>>>Personas que han entrado: " + str(contadorEntrada))
        print(">>>>>>>>>>>>>>>>Personas que han salido: " + str(contadorSalida))
        print(">>>>>>>>>>>>>>>> [" + str(actualDay) + "]-Personas que han entrado HOY: " + str(contadorEntradaHoy))
        print(">>>>>>>>>>>>>>>> [" + str(actualDay) + "]-Personas que han salido HOY: " + str(contadorSalidaHoy))

        response = requests.get('https://api.thingspeak.com/update?api_key=G5UQKN1ETHTYMWEA&field1=' + str(contadorDentro))
        response = requests.get('https://api.thingspeak.com/update?api_key=G5UQKN1ETHTYMWEA&field2=' + str(contadorEntrada))
        response = requests.get('https://api.thingspeak.com/update?api_key=G5UQKN1ETHTYMWEA&field3=' + str(contadorSalida))
        response = requests.get('https://api.thingspeak.com/update?api_key=G5UQKN1ETHTYMWEA&field4=' + str(contadorEntradaHoy))
        response = requests.get('https://api.thingspeak.com/update?api_key=G5UQKN1ETHTYMWEA&field5=' + str(contadorSalidaHoy))

    my_semaphore.release()

    print(threading.current_thread().getName() + "::: semaforo soltado :" + str(entrando) + " - " + str(saliendo))


def salida(howmany):
    global my_semaphore
    global entrando
    global saliendo
    global contadorEntrada
    global TimeStamp
    global inc
    global contadorEntradaHoy
    global contadorSalidaHoy
    global actualDay
    global contadorDentro

    print(threading.current_thread().getName() + "::: EMPEZANDO")

    my_semaphore.acquire()

    print(threading.current_thread().getName() + "::: semaforo adquirido")

    if not entrando and not saliendo:
        saliendo = True
        TimeStamp = time.time()
        apertura()
        inc = howmany

    if entrando and not saliendo:
        entrando = False
        if newDay() is True:
            contadorEntradaHoy = 0
            contadorSalidaHoy = 0
            contadorDentro = 0
            actualDay = date.today()
        contadorEntrada += inc
        contadorEntradaHoy += inc
        contadorDentro += inc

        print(">>>>>>>>>>>>>>>> [" + str(actualDay) + "]-Personas dentro del edificio: " + str(contadorDentro))
        print(">>>>>>>>>>>>>>>>Personas que han entrado: " + str(contadorEntrada))
        print(">>>>>>>>>>>>>>>>Personas que han salido: " + str(contadorSalida))
        print(">>>>>>>>>>>>>>>> [" + str(actualDay) + "]-Personas que han entrado HOY: " + str(contadorEntradaHoy))
        print(">>>>>>>>>>>>>>>> [" + str(actualDay) + "]-Personas que han salido HOY: " + str(contadorSalidaHoy))

        response = requests.get('https://api.thingspeak.com/update?api_key=G5UQKN1ETHTYMWEA&field1=' + str(contadorDentro))
        response = requests.get('https://api.thingspeak.com/update?api_key=G5UQKN1ETHTYMWEA&field2=' + str(contadorEntrada))
        response = requests.get('https://api.thingspeak.com/update?api_key=G5UQKN1ETHTYMWEA&field3=' + str(contadorSalida))
        response = requests.get('https://api.thingspeak.com/update?api_key=G5UQKN1ETHTYMWEA&field4=' + str(contadorEntradaHoy))
        response = requests.get('https://api.thingspeak.com/update?api_key=G5UQKN1ETHTYMWEA&field5=' + str(contadorSalidaHoy))

    my_semaphore.release()

    print(threading.current_thread().getName() + "::: semaforo soltado :" + str(entrando) + " - " + str(saliendo))


def receiveNotification(port):
    global howmany

    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the port
    # server_address = ('158.49.245.45', port)
    server_address = ('0.0.0.0', port)
    print('starting up on %s port %s \n' % server_address)
    sock.bind(server_address)

    LastTime = time.time()

    # Listen for incoming connections
    sock.listen(1)
    threadName = threading.current_thread().getName()

    while True:
        # Wait for a connection
        print('waiting for a connection in %s\n' % threadName)
        connection, client_address = sock.accept()

        NowTime = time.time()
        dif = NowTime - LastTime
        print("La diferencia de tiempo es :" + str(dif))

        if dif > 5:
            print('connection from ', client_address)

            data = connection.recv(16)
            print('received connection to ', threadName)

            if data:
                urllib.urlretrieve('http://juanher:juanher!12@158.49.245.71:27654/jpg/image.jpg',
                                   'image' + threadName + '.jpg')
                # os.system('/usr/bin/python python-image-resizer.py')
                # with open("peq/image" + threadName.lower() + ".jpg", 'r+') as f:
                print("Cogiendo imagen de la camara")
                with open("image" + threadName + ".jpg", 'r+') as f:
                    jpgdata = f.read()
                    f.close()

                    image_64_encode = base64.b64encode(jpgdata)
                    try:
                        response = requests.post('http://qss.unex.es:2080', data=image_64_encode)
                        # print (response.text)

                        if 'person' in response.text:
                            howmany = (response.text).count('person')
                            print(">Persona detectada --> " + str(howmany) + " personas detectadas")

                            # We save the image to be used in the DataSet of persons
                            copy('image' + threadName + '.jpg', 'Dataset/image' + str(time.time()) + '.jpg')

                            if port == 28282:
                                entrada(howmany)
                            elif port == 28283:
                                salida(howmany)
                            LastTime = NowTime

                    except:
                        print(">> NO se ha podido contactar con PyYolo")

            # thread.sleep(2) #sleep two seconds, because we want to wait the person is entering or exiting, complete his action
        connection.close()


def saliendoThread():
    global pid1
    pid1 = os.getpid()
    receiveNotification(28283)


def entrandoThread():
    global pid2
    pid2 = os.getpid()
    receiveNotification(28282)


if __name__ == "__main__":
    global pid1
    global pid2
    global pid3
    global actualDay

    actualDay = date.today()

    r = threading.Thread(target=reset, name='ResetThread')
    t = threading.Thread(target=saliendoThread, name='SSSaliendo')
    w = threading.Thread(target=entrandoThread, name='SSEntrando')

    r.start()
    w.start()
    t.start()

    signal.signal(signal.SIGINT, my_handler)

    signal.pause()

    print("Killing '" + w.getName() + "' process (entrando)")
    print(">>Ident:" + str(w.ident) + ". PID:" + str(pid2) + ".")
    os.kill(pid2, signal.SIGTERM)
    print("Killing '" + t.getName() + "' process (saliendo)")
    print(">>Ident:" + str(t.ident) + ". PID:" + str(pid1) + ".")
    os.kill(pid1, signal.SIGTERM)
    print("Killing '" + r.getName() + "' process (reset)")
    print(">>Ident:" + str(r.ident) + ". PID:" + str(pid3) + ".")
    os.kill(pid3, signal.SIGTERM)

    # r.join()
    # t.join()
    # w.join()



